﻿using BackButtonMadness.Annotations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BackButtonMadness.ViewModels
{
  public class MockLoginViewModel : INotifyPropertyChanged
  {
    public string Password { get; set; }

    public Command SubmitLogin
    {
      get
      {
        return new Command(() =>
        {
          var viewModel = new MockContentViewModel1 { MainText = "Navigated to MockContentPage1 from MockLogin." };
          var page = new MockContentPage1 { BindingContext = viewModel };
          DependencyContainer.Resolve<NavigationPage>().PushAsync(page);
        });
      }
    }

    public string Username { get; set; }

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      var handler = PropertyChanged;
      if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}