﻿using BackButtonMadness.Annotations;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace BackButtonMadness.ViewModels
{
  public class MockContentViewModel1 : INotifyPropertyChanged
  {
    private string _mainText;

    public string MainText
    {
      get { return _mainText; }
      set { _mainText = value; }
    }

    public Command NextPage
    {
      get
      {
        return new Command(() =>
        {
          var viewModel = new MockContentViewModel2 { MainText = "Navigated to MockContentPage2 from MockContentPage1." };
          var page = new MockContentPage2 { BindingContext = viewModel };
          DependencyContainer.Resolve<NavigationPage>().PushAsync(page);
        });
      }
    }

    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      var handler = PropertyChanged;
      if(handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}