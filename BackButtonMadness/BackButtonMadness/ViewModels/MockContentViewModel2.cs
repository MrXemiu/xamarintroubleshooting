﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using BackButtonMadness.Annotations;

namespace BackButtonMadness.ViewModels
{
  public class MockContentViewModel2 : INotifyPropertyChanged
  {
    public string MainText { get; set; }
    public event PropertyChangedEventHandler PropertyChanged;

    [NotifyPropertyChangedInvocator]
    protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
    {
      var handler = PropertyChanged;
      if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
    }
  }
}