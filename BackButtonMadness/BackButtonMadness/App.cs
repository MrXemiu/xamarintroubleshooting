﻿using BackButtonMadness.ViewModels;
using Xamarin.Forms;

namespace BackButtonMadness
{
  public class App : Application
  {
    public App()
    {
      var firstPage = new MockLogin { BindingContext = new MockLoginViewModel() };

      MainPage = new NavigationPage(firstPage);

      //make MainPage "universally" available but loosely coupled
      DependencyContainer.Register((NavigationPage)MainPage);
    }

    protected override void OnResume()
    {
      // Handle when your app resumes
    }

    protected override void OnSleep()
    {
      // Handle when your app sleeps
    }

    protected override void OnStart()
    {
      // Handle when your app starts
    }
  }
}