﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Autofac.Core.Registration;

namespace BackButtonMadness
{
  public static class DependencyContainer
  {
    private static IContainer Container { get; set; }

    static DependencyContainer()
    {
      Container = new ContainerBuilder().Build();
    }

    public static T Resolve<T>()
    {
      return Container.Resolve<T>();
    }

    public static void Register<T>(T obj) where T : class
    {
      var builder = new ContainerBuilder();
      builder.Register<T>(t => obj);
      builder.Update(Container);
    }

    public static void Register<T, TImpl>()where T : class where TImpl : class, T
    {
      var builder = new ContainerBuilder();
      builder.RegisterType<TImpl>().As<T>();
      builder.Update(Container);
    }
  }
}
