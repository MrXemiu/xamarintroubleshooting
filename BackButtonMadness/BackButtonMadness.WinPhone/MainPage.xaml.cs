﻿using Microsoft.Phone.Controls;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Platform.WinPhone;

namespace BackButtonMadness.WinPhone
{
  public partial class MainPage : FormsApplicationPage
  {
    public MainPage()
    {
      InitializeComponent();
      SupportedOrientations = SupportedPageOrientation.PortraitOrLandscape;

      Forms.Init();
      LoadApplication(new BackButtonMadness.App());
    }

    protected override async void OnBackKeyPress(CancelEventArgs e)
    {
      var currentPage = DependencyContainer.Resolve<NavigationPage>().CurrentPage;

      Device.BeginInvokeOnMainThread(async () =>
      {
        var message = string.Format("Click 'Continue' to navigate away from {1}.{0}Click 'Cancel' to cancel backward navigation.",
          System.Environment.NewLine, currentPage.Title);

        var response = await currentPage.DisplayActionSheet(message, "Cancel", "Continue");
        e.Cancel = response.Equals("No");
      });
    }
  }
}